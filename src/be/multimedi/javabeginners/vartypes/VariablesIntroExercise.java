package be.multimedi.javabeginners.vartypes;

public class VariablesIntroExercise {
    public static void main(String[] args) {
        String firstName = "Ben";
        String lastName = "Couwberghs";
        String fullName = firstName + ' ' + lastName;
        int age = 23;
        System.out.println("Hello, I am " + fullName + " and I am " + age + " years old.");
        System.out.println("Good evening, Mr. " + lastName + ".");
        System.out.println("Please, call me " + firstName + ".");
        System.out.println("So, " + firstName + ", in a decade you will be " + (age + 10) + " years old!");
        System.out.println("Yes, but for now I'm still " + age + " years young.");
    }
}
