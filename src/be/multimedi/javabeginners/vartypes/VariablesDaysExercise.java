package be.multimedi.javabeginners.vartypes;

import java.util.Scanner;

public class VariablesDaysExercise {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter your age (in years): ");
        int age = keyboard.nextInt();
        int ageDays = age * 365;
        System.out.printf("I am %,d days old!", ageDays);
    }
}
