package be.multimedi.javabeginners.vartypes;

public class VariablesTemperatureExercise {
    public static void main(String[] args) {
        double tempCelsius = 18.5;
        double tempFahrenheit = tempCelsius * 9 / 5 + 32;
        System.out.println(tempCelsius + "°C is " + tempFahrenheit + "°F");
    }
}
