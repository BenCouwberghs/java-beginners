package be.multimedi.javabeginners.operators;

import java.util.Random;
import java.util.Scanner;

public class MathsExercise {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random rand = new Random();
        int a = rand.nextInt(101) -1;
        int b = rand.nextInt(101) -1;

        System.out.printf("What is %d + %d?\n", a, b);
        int guess = scanner.nextInt();
        if (guess == (a + b)) {
            System.out.println("Correct! You get 1 point");
        } else {
            System.out.println("Wrong...the correct answer is " + (a + b));
        }
    }
}
