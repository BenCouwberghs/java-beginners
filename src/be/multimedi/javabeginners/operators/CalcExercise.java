package be.multimedi.javabeginners.operators;

import java.util.Scanner;

public class CalcExercise {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter your weight (in kg): ");
        double weight = keyboard.nextDouble();
        System.out.print("Enter your height (in m): ");
        double height = keyboard.nextDouble();
        double bmi = weight / Math.pow(height, 2);

        if (bmi > 35) {
            System.out.println("with a BMI of " + bmi + " you're extremely obese");
        } else if (bmi > 30) {
            System.out.println("with a BMI of " + bmi + " you're obese");
        } else if (bmi > 25) {
            System.out.println("with a BMI of " + bmi + " you're overweight");
        } else if (bmi > 18.5) {
            System.out.println("with a BMI of " + bmi + " you're normal");
        } else {
            System.out.println("with a BMI of " + bmi + " you're underweight");
        }
    }
}
