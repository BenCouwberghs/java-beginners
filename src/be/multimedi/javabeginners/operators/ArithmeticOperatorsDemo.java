package be.multimedi.javabeginners.operators;

import java.util.Scanner;

public class ArithmeticOperatorsDemo {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int a = keyboard.nextInt();
        System.out.print("Enter another number: ");
        int b = keyboard.nextInt();
        System.out.printf("a: %d, b: %d\n", a, b);
        System.out.printf("%d + %d = %d\n", a, b, a + b);
        System.out.printf("%d - %d = %d\n", a, b, a - b);
        System.out.printf("%d x %d = %d\n", a, b, a * b);
        System.out.printf("%d / %d = %f\n", a, b, (double)a / b);
        System.out.printf("%d %% %d = %d\n", a, b, a + b);
        System.out.printf("++%d = %d\n", a , ++a);
        System.out.printf("%d++ = %d\n", a , a++);
        System.out.printf("--%d = %d\n", b , --b);
        System.out.printf("%d-- = %d\n", b , b--);
    }
}
