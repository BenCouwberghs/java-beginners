package be.multimedi.javabeginners.operators;

import java.util.Scanner;

public class PostageExercise {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Voer uw briefgewicht in (g): ");
        int weight = scanner.nextInt();
        int stampNumber = 0;
        if (weight < 100) {
            stampNumber = 2;
        } else if (weight < 350) {
            stampNumber = 3;
        } else if (weight < 1_000) {
            stampNumber = 5;
        } else {
            stampNumber = 7;
        }

        System.out.printf("Je hebt %d aantal zegels nodig op je brief", stampNumber);
    }
}
