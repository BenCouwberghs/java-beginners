package be.multimedi.javabeginners.operators;

import java.util.Objects;
import java.util.Scanner;

public class FavouritismExercise {
    public static void main(String[] args) {
        String favouriteName = "James";
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter your first name: ");
        String name = scanner.next();

        if (Objects.equals(name, favouriteName)) {
            System.out.printf("%s!? What a cool name!!", name);
        } else {
            System.out.printf("Hm...%s...Yeah, that's a name alright...", name);
        }
    }
}
