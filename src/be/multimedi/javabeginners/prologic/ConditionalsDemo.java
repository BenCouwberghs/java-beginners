package be.multimedi.javabeginners.prologic;
import java.util.Scanner;

public class ConditionalsDemo {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter your age: ");
        int age = keyboard.nextInt();
        if (age >= 18) {
            System.out.println("Adult");
        } else if(age >= 10) {
            System.out.println("Teenager");
        } else if(age >= 2) {
            System.out.println("Child");
        } else {
            System.out.println("Baby");
        }
    }
}
