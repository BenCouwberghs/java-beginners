package be.multimedi.javabeginners.prologic; // We need to import the Scanner-class if we want to use it in our code here

import java.util.Scanner;

public class InputDemo {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in); // A Scanner-object through which we can ask for user input
        System.out.println("Enter a number:");
        int a = keyboard.nextInt(); // Input the first number
        System.out.println("Enter another number:");
        int b = keyboard.nextInt(); // Int put the second number
        int sum = a + b;
        System.out.println("The sum is: " + sum); // Output the resulting sum
    }
}
