package be.multimedi.javabeginners.prologic;

public class OutputDemo {
    public static void main(String[] args) {
        System.out.println("Hello world");
        System.out.println("Hola " + "mundo"); // Using + in text combines them (concatenation)
        System.out.print("Bonjour monde"); // Using print without ln removes the newline ('\n')
        System.out.println("Hallo wereld"); //  Will appear next to previous print because it withheld a newline

    }
}
