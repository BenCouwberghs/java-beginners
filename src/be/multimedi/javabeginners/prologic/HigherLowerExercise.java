package be.multimedi.javabeginners.prologic;

import java.util.Random;
import java.util.Scanner;

public class HigherLowerExercise {
    public static void main(String[] args) {

        Random random = new Random();
        int randomNumber = random.nextInt(100) + 1;


        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a guess: ");
        int answer = keyboard.nextInt();
        while (answer != randomNumber) {
            System.out.print("Enter another guess: ");
            answer = keyboard.nextInt();

            if (answer > randomNumber) {
                System.out.println("Lower!");
            } else if (answer < randomNumber) {
                System.out.println("Higher!");
            } else {
                System.out.println("Guessed");
            }
        }
    }
}
