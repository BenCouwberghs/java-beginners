package be.multimedi.javabeginners.prologic;

import com.sun.source.tree.WhileLoopTree;

import java.util.Scanner;

public class LoopsDemo {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Add flour");
        System.out.println("Add milk");
        System.out.println("Add eggs");
        System.out.println("Mix");
        System.out.print("Are you hungry? (y/n): ");
        String answer = keyboard.nextLine();

        while (answer.equals("y")) {
            System.out.println("Baking pancake...");
            System.out.println("Done! Bon apetit \uD83E\uDD5E");
            System.out.print("Are you still hungry? (y/n):");
            answer = keyboard.nextLine();
        }
        System.out.println("Finished making pancakes!");
    }
}
