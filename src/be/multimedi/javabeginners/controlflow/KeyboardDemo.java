package be.multimedi.javabeginners.controlflow;

public class KeyboardDemo {
    public static void main(String[] args) {
        String name = KeyboardUtility.askForString("Enter your name: ");
//        System.out.println("Hello, " + name);
        int age = KeyboardUtility.askForInt("Enter your age: ");
//        System.out.println("I am " + age + " years old");
        double height = KeyboardUtility.askForDouble("Enter your height (m): ");
//        System.out.println("I'm " + height + "m tall");

        System.out.printf("Hello. my name is %s, I am %d years old and I'm %.2fm tall.", name, age, height);
    }
}