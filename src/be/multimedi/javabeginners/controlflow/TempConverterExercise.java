package be.multimedi.javabeginners.controlflow;

import java.util.Scanner;

public class TempConverterExercise {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Choose temperature conversion (C to F/F to C): ");
        String tempConverter = scanner.nextLine();
        System.out.print("Enter a temperature: ");
        double temp = scanner.nextDouble();

        if (tempConverter.equals("C to F")) {
            temp = convertCToF(temp);
            System.out.printf("The converted temperature is %.1f degrees Fahrenheit", temp);
        } else if (tempConverter.equals("F to C")) {
            temp = convertFToC(temp);
            System.out.printf("The converted temperature is %.1f degrees Celsius", temp);
        }
    }

    private static double convertCToF(double tempCelsius) {
        return tempCelsius * ((double) 9 / 5) + 32;
    }

    private static double convertFToC(double tempFahrenheit) {
        return (tempFahrenheit - 32) * ((double) 5 / 9);
    }
}
