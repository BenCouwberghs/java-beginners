package be.multimedi.javabeginners.controlflow;

public class UnluckyThirteenExercise {
    public static void main(String[] args) {
        for (int i = 1; i <= 82; i++) {
            if (i != 13) {
                System.out.printf("Floor %d:\n", i);
                for (int j = 1; j <= 28; j++) {
//                    if (j != 13) {
//                        System.out.println("Cleaning Room " + j);
//                    }
                    if (j % 2 != 0) {
                        System.out.printf("Cleaning Room %dA\n", (j / 2 + j % 2));
                    } else {
                        System.out.printf("Cleaning Room %dB\n", j / 2);
                    }
                }
            }

            if (i == 42) {
                System.out.println("I'm really tired of walking up all these flights of stairs...I'm gonna take a siesta");
                break;
            }
        }
    }
}
