package be.multimedi.javabeginners.controlflow;

import java.util.Scanner;

public class KeyboardUtility {
    private static final Scanner KEYBOARD = new Scanner(System.in);

    public static String askForString(String question) {
        System.out.print(question);
        return KEYBOARD.nextLine();
    }

    public static int askForInt(String question) {
        return Integer.parseInt(askForString(question));
    }

    public static Double askForDouble(String question) {
        System.out.print(question);
        return Double.parseDouble(askForString(question));
    }
}
